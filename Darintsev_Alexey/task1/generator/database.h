#ifndef DATABASE_H
#define DATABASE_H

#include <QDateTime>
using namespace std;

void set_seed (int seed_val);   //зерно для рандомайзера
string date_input (void);       //запрос даты/времени

/* создаёт меняющуюся по закону sin зависимость
 * со случайной составляющей, величина которой
 * зависит от модели датчика */
class Data_randomiser
{
    float temp_period;
    float temp_amplitude;
    float temp_mid;
    float hum_period;
    float hum_amplitude;
    float hum_mid;
    float random;

public:
    Data_randomiser (unsigned char model_num);
    float temp_random (unsigned int time);
    float hum_random (unsigned int time);
};

/* генерирует случайные параметры для сенсора
 * и подготавливает SQL команду для записи
 * этих параметров в БД */
class Sensor_parametrs
{
    QString model;
    QDateTime inst_date;
    QDate date_NULL;
    QTime time_NULL;
    QDateTime last_rec;
    QString str1;
    QString str2;
    QString str3;
    QString str4;
    QString str5;
    QString str6;
    float latitude;
    float flongitude;
    unsigned int step_number;
    unsigned int step;
    QString qsql;

public:
    unsigned char model_num;
    Sensor_parametrs (unsigned int step_number,  unsigned int step);
    string sql;
};

/* для промежуточного хранения данных */
struct Output_data
{
    string temp;
    string hum;
};

/* формат вывода данных для дальнейшей обработки */
class Output_data_full
{
public:
    string max_temp;
    string min_temp;
    string avg_temp;
    string max_hum;
    string min_hum;
    string avg_hum;
    void input (unsigned char command, Output_data data);
};

/* Класс БД и необходимый набор методов по
 * работе с ней */
class Database
{
    sqlite3 *database;
    string db_name_str;
    void send_to_db_QStr (const QString sql_str);
    Output_data search_data (unsigned int sensor_N, QString command, quint64 target_start_time_sec, quint64 target_end_time_sec);

public:
    Database (string name);
    void send_to_db (string str);          //обработка готового SQL запрса
    void create(unsigned int sensor_num);  //создание БД
    void open(void);                       //открытие существующей БД
    void sensor_list (void);               //вывод списка датчиков и их характеристик
    void data_input(unsigned int sensor_number, unsigned int time, float temperature, float humidity);  //запись данных с датчика
    void close (void); //корректное завершение работы с БД

    Output_data_full data_between (unsigned int sensor_N, string date1, string date2); //возвращает min, max и avg значения
                                                                                       //температуры и влажности в заданном интервале времени
};

#endif
