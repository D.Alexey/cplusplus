#include <iostream>
#include <QDateTime>
#include <QRandomGenerator>
#include "sqlite3.h"
#include "database.h"

using namespace std;

const QString models [3] = {"THS1", "THS10", "THS100"}; //модели датчиков, имеют разную "погрешность"
const QString commands [3] = {"MAX", "MIN", "AVG"};      //для формирования SQL запросов
const string DMYHMS [6] = {"Day (DD): ", "Month (MM): ", "Year(YYYY): ", "Hour(HH): ", "Minute(MM): ", "Second(SS): "}; //для запроса времени
QRandomGenerator qrandom;

void set_seed (int seed_val)
{
    qrandom.seed(seed_val);
}

Data_randomiser::Data_randomiser (unsigned char model_num)
{
    temp_period = qrandom.bounded(100.0);
    temp_amplitude = qrandom.bounded(5.0);
    temp_mid = qrandom.bounded(30.0);
    hum_period = qrandom.bounded(100.0);
    hum_amplitude = qrandom.bounded(5.0);
    hum_mid = qrandom.bounded(50.0) + 25;
    random = pow(10, (-1 * model_num));
}

float Data_randomiser::temp_random (unsigned int time)
{
    float val = temp_amplitude * sin(time / temp_period) + temp_mid + random * qrandom.generateDouble();
    return val;
}

float Data_randomiser::hum_random (unsigned int time)
{
    float val = hum_amplitude * sin(time / hum_period) + hum_mid + random * qrandom.generateDouble();
    return val;
}

Sensor_parametrs::Sensor_parametrs (unsigned int step_number,  unsigned int step)
{
    date_NULL.setDate(2000, 1, 1);
    inst_date.setDate(date_NULL);
    time_NULL.setHMS (0, 0, 0);
    inst_date.setTime(time_NULL);
    model_num = qrandom.bounded(3);
    model = models[model_num];
    inst_date = inst_date.addMonths(qrandom.bounded(120));
    inst_date = inst_date.addDays(qrandom.bounded(365));
    last_rec = inst_date.addSecs(step_number * step);
    str1 = inst_date.toString("dd.MM.yyyy hh:mm:ss");
    str2 = last_rec.toString("dd.MM.yyyy hh:mm:ss");
    str3 = model;
    latitude = qrandom.bounded(180.0) - 90;
    flongitude = qrandom.bounded(360.0) - 180;
    str4 = QString::number(latitude);
    str5 = QString::number(flongitude);
    str6 = QString::number(step);
    qsql = "INSERT INTO sensor_list (install_date, last_rec, model, latitude, flongitude, step) VALUES ('" + str1 + "', '" + str2 + "', '" + str3 + "', '" + str4 + "', '" + str5 + "', '" + str6 + "');";
    sql = qsql.toUtf8().constData();
}

void Output_data_full::input (unsigned char command, Output_data data)
{
    switch (command)
    {
        case 0:
            max_temp = data.temp;
            max_hum = data.hum;
            break;
        case 1:
            min_temp = data.temp;
            min_hum = data.hum;
            break;
        case 2:
            avg_temp = data.temp;
            avg_hum = data.hum;
            break;
    }
}

Database::Database (string name)
{
    db_name_str = name;
}

void Database::send_to_db (string str)
{
    const char *sql_char = str.c_str();
    char *zErrMsg = 0;
    sqlite3_exec(database, sql_char, NULL, 0, &zErrMsg);
}

void Database::send_to_db_QStr (const QString sql_str)
{
    QByteArray qchar = sql_str.toLocal8Bit();
    const char *sql_char = qchar.data();
    char *zErrMsg = 0;
    sqlite3_exec(database, sql_char, NULL, 0, &zErrMsg);
}

void Database::create (unsigned int sensor_num)
{
    const char *db_name = db_name_str.c_str();
    sqlite3_open (db_name, &database);
    send_to_db_QStr ("CREATE TABLE sensor_list (id INTEGER PRIMARY KEY, install_date TEXT, last_rec TEXT, model TEXT, latitude FLOAT, flongitude FLOAT, step INT);");
    for (unsigned int i = 1; i <= sensor_num; i++)
    {
        const QString num = QString::number(i);
        QString sql = "CREATE TABLE sensor" + num + " (id INTEGER PRIMARY KEY, time INT, temperature FLOAT, humidity FLOAT);";
        send_to_db_QStr (sql);
    }
}

void Database::open (void)
{
    const char *db_name = db_name_str.c_str();
    sqlite3_open (db_name, &database);
};

void Database::data_input(unsigned int sensor_number, unsigned int time, float temperature, float humidity)
{
    const QString number = QString::number(sensor_number);
    const QString tm = QString::number(time);
    const QString temp = QString::number(temperature);
    const QString humd = QString::number(humidity);
    QString sql = "INSERT INTO sensor" + number + " (time, temperature, humidity) VALUES ('" + tm + "', '" + temp + "', '" + humd + "');";
    send_to_db_QStr(sql);
}

void Database::sensor_list (void)
{
    sqlite3_stmt *res;
    const char *tail;
    sqlite3_prepare_v2(database,"SELECT install_date, last_rec, model, latitude, flongitude, step from sensor_list order by id", 1000, &res, &tail);

    unsigned int rec_count = 1;

    while (sqlite3_step(res) == SQLITE_ROW) //пока выделена готова к воводу запись...
    {
        cout << rec_count << " ";
        cout << sqlite3_column_text(res, 0) << " ";
        cout << sqlite3_column_text(res, 1) << " ";
        cout << sqlite3_column_text(res, 2) << " ";
        cout << sqlite3_column_text(res, 3) << " ";
        cout << sqlite3_column_text(res, 4) << " ";
        cout << sqlite3_column_text(res, 5) << endl;
        rec_count++;
    }
}

Output_data Database::search_data (unsigned int sensor_N, QString command, quint64 target_start_time_sec, quint64 target_end_time_sec) //private
{
    sqlite3_stmt *res;
    const char *tail;
    QString data_out = "SELECT " + command + " (temperature), " + command + " (humidity) FROM sensor" + QString::number(sensor_N) + " WHERE time BETWEEN " + QString::number(target_start_time_sec) + " and " + QString::number(target_end_time_sec);
    QByteArray qchar = data_out.toLocal8Bit();
    const char *sql_char = qchar.data();
    sqlite3_prepare_v2(database, sql_char, 1000, &res, &tail);
    sqlite3_step(res);
    Output_data data;
    data.temp = string(reinterpret_cast<const char*>(sqlite3_column_text(res, 0))); //sqlite3_column_text возвращает unsigned char*
    data.hum = string(reinterpret_cast<const char*>(sqlite3_column_text(res, 1)));
    return data;
}

Output_data_full Database::data_between (unsigned int sensor_N, string date1, string date2)
{
    QDateTime target_start_time = QDateTime::fromString(QString::fromUtf8(date1.c_str()), "dd.MM.yyyy hh:mm:ss"); //преобразование нужно, т.к. QDateTime работает только с QString
    QDateTime target_end_time = QDateTime::fromString(QString::fromUtf8(date2.c_str()), "dd.MM.yyyy hh:mm:ss");
    /* Получение даты установки датчика */
    string sql_install_date_str = "SELECT install_date from sensor_list WHERE id = " + to_string(sensor_N);
    const char * sql_install_date = sql_install_date_str.c_str();
    sqlite3_stmt *res;
    const char *tail;
    sqlite3_prepare_v2(database, sql_install_date, 1000, &res, &tail);
    sqlite3_step(res);
    /* пересчёт введённых пользователем абсолютных дат в относительное время,
     * так как данные с датчиков хранятся с привязкой к относительному времени
     * (сколько секунд прошлло с начала работы датчика) */
    QString start_time_qstr = QString::fromUtf8(std::string(reinterpret_cast<const char*>(sqlite3_column_text(res, 0))).c_str());
    QDateTime start_time = QDateTime::fromString(start_time_qstr, "dd.MM.yyyy hh:mm:ss");
    quint64 target_start_time_sec = start_time.secsTo(target_start_time);
    quint64 target_end_time_sec = target_start_time.secsTo(target_end_time);
    /* Обращение к БД 3 раза с командами min, max и avg по заданному интервалу времени */
    Output_data_full data_full;
    for (unsigned char i = 0; i <= 2; i++)
    {
        Output_data data_part;
        data_part = search_data(sensor_N, commands[i], target_start_time_sec, target_end_time_sec);
        data_full.input(i, data_part);
    }
    return data_full;
}

void Database::close (void)
{
        sqlite3_close(database);
}

string date_input (void)
{
    string str;
    string temp;
    for (unsigned char i = 0; i <= 5; i++)
    {
        cout << DMYHMS[i];
        cin >> temp;
        str = str + temp;
        if (i < 2) str = str + ".";
        if (i == 2) str = str + " ";
        if (i > 2 and i < 5) str = str + ":";
    }
    return str;
}
