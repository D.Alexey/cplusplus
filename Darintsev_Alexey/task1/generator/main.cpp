#include <iostream>
#include "sqlite3.h"
#include "database.h"

using namespace std;

int main(void)
{
    cout << "Seed for randomiser: ";
    int seed;
    cin >> seed;
    set_seed(seed);

    cout << "Db file name (*.db): ";
    string db_name;
    cin >> db_name;
    Database sensor_db (db_name);
    cout << "Number of sensors: "; //количество датчиков
    unsigned int sens_number;
    cin >> sens_number;
    sensor_db.create(sens_number); //создание файла БД

    for (unsigned int num = 1; num <= sens_number; num++)
    {
        cout << "Sensor " << num << "Number of steps: ";
        unsigned int step_number; //количество записей (измерений)
        cin >> step_number;
        cout << "Sensor " << num << " step size (sec): ";
        unsigned int step_size; //шаг между измерениями
        cin >> step_size;
        Sensor_parametrs sensor (step_number, step_size); //остальные параметры датчика случайные
        sensor_db.send_to_db(sensor.sql);
        Data_randomiser randomiser(sensor.model_num);

        for (unsigned int time = 0; time <= step_number * step_size; time = time + step_size)
        {
            float temp = randomiser.temp_random(time); //показания меняются по закону sin + случайное значение, в зависимости от модели датчика
            float hum = randomiser.hum_random(time);
            sensor_db.data_input(num, time, temp, hum);
        }
        cout << "Sensor " << num << " is ready" << endl;
    }

    sensor_db.close();
    return 0;
}
