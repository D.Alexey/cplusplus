#include <iostream>
#include "../generator/sqlite3.h"
#include "../generator/database.h"

using namespace std;

int main(void)
{
    cout << "Db file name: ";
    string db_name;
    cin >> db_name;
    Database sensor_db (db_name);

    sensor_db.open();
    sensor_db.sensor_list(); //вывод списка датчиков

    cout << "Sensor number: ";
    unsigned int sens_number;
    cin >> sens_number;
    cout << "Start time:" << endl;
    string start_time = date_input();
    cout << "End time: " << endl;
    string end_time = date_input();

    Output_data_full data = sensor_db.data_between (sens_number, start_time, end_time);
    cout << "max temp: " << data.max_temp << " min temp: " << data.min_temp << " avg temp: " << data.avg_temp << endl;
    cout << "max hum: " << data.max_hum << " min hum: " << data.min_hum << " avg hum: " << data.avg_hum << endl;
    sensor_db.close();

    return 0;
}


